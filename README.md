# Software Studio 2020 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Name of widgets                                  | 1~5%     | N         |


---

### How to use 

    進入小畫家的頁面後，就能看到各式的繪圖工具。每個按鈕上的單字則直接對應到它所屬的功能，
    其中的Rect以及三個Fill案件則代表矩形、實心矩形、實心圓形和實心三角形的繪圖功能。
    長條空白處則是提供文字輸入的地方，並在右方有調整筆刷及橡皮擦大小的slide。

### Function description

    同時，在Canvas下方有著Redo及Undo的功能鍵，並且在其左上角也能看見download和upload
    圖片的對應鍵，bonus部分的實心圖形則在上方介紹過。
### Gitlab page link

    https://107062221.gitlab.io/AS_01_WebCanvas

### Others (Optional)

    於實作鼠標圖案的過程中遭遇不明的錯誤，格式正確卻無法正常顯示，導致無法順利完成這個項目，
    也因為時間因素也無法完成字體大小及字型的選擇功能。雖說這次的homework讓我著實學到不少，
    但仍感到十分沮喪。

<style>
table th{
    width: 100%;
}
</style>