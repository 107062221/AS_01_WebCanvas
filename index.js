var canvas = document.querySelector("canvas");
canvas.height = window.innerHeight - 300;
canvas.width = window.innerWidth - 400;
var draw = canvas.getContext("2d");

let paint = false;
let drawType = "default";
var startX, startY;
let flag = false;

canvas.addEventListener("mousedown", IsPainting);
canvas.addEventListener("mouseup", NotPainting);

function IsPainting(cursor){
    if(flag == false){
        Push();
        flag = true;
    }
    paint = true;
    startX = cursor.clientX;
    startY = cursor.clientY;
    draw.beginPath();
    canvas.addEventListener("mousemove", Drawing);
}

function NotPainting(){
    paint = false;
    draw.globalCompositeOperation = "source-over";
    Push();
}

function CleanCanvas(){
    draw.clearRect(0,0,canvas.width,canvas.height);
    console.log("clear!");
    Push();
}

var canvasArray = new Array();
var step = -1;

function Push(){
    ++step;
    if(step < canvasArray.length){
        canvasArray.length = step;
    }
    canvasArray.push(canvas.toDataURL());
    console.log("pushed");
}

function Undo(){
    if(step > 0){
        --step;
        var canvasImage = new Image();
        canvasImage.src = canvasArray[step];
        canvasImage.onload = function(){
            draw.clearRect(0,0,canvas.width,canvas.height);
            draw.drawImage(canvasImage,0,0);
        }
        console.log("undo!");
    }
}

function Redo(){
    if(step < canvasArray.length-1){
        ++step;
        var canvasImage = new Image();
        canvasImage.src = canvasArray[step];
        canvasImage.onload = function(){
            draw.clearRect(0,0,canvas.width,canvas.height);
            draw.drawImage(canvasImage,0,0);
        }
        console.log("redo!");
    }
}

function changeColor(newColor){
    draw.strokeStyle = newColor;
    draw.fillStyle = newColor;
}

function Drawing(cursor){
    if(!paint) return;
    else if(drawType == "default") return;

    else if(cursor.clientX < 200 || cursor.clientX > canvas.width+196) NotPainting();
    else if(cursor.clientY < 13 || cursor.clientY > canvas.height+8) NotPainting();

    console.log("drawing now!");
    if(drawType == "pencil"){
        draw.lineWidth = document.getElementById("brushSize").value;
        draw.lineCap = "round";

        draw.lineTo(cursor.clientX-200, cursor.clientY-13);
        draw.stroke();
        draw.beginPath();
        draw.moveTo(cursor.clientX-200, cursor.clientY-13);
    }

    if(drawType == "eraser"){
        draw.globalCompositeOperation = "destination-out";
        draw.lineWidth = document.getElementById("eraserSize").value;
        draw.lineCap = "round";
    
        draw.lineTo(cursor.clientX-200, cursor.clientY-13);
        draw.stroke();
        draw.beginPath();
        draw.moveTo(cursor.clientX-200, cursor.clientY-13);
    }

    if(drawType == "line"){
        draw.lineWidth = document.getElementById("brushSize").value;
        draw.lineCap = "round";

        draw.beginPath();
        draw.clearRect(0,0,canvas.width,canvas.height);
        var canvasImage = new Image();
        canvasImage.src = canvasArray[step];
        draw.drawImage(canvasImage,0,0);

        draw.moveTo(startX-200, startY-13);
        draw.lineTo(cursor.clientX-200, cursor.clientY-13);
        draw.stroke();
        draw.closePath();
    }

    if(drawType == "rect"){
        draw.lineWidth = document.getElementById("brushSize").value;

        draw.beginPath();
        draw.clearRect(0,0,canvas.width,canvas.height);
        var canvasImage = new Image();
        canvasImage.src = canvasArray[step];
        draw.drawImage(canvasImage,0,0);

        draw.rect(startX-200,startY-13,cursor.clientX-startX,cursor.clientY-startY);
        draw.stroke();
    }

    if(drawType == "circle"){
        draw.lineWidth = document.getElementById("brushSize").value;
        draw.lineCap = "round";

        draw.beginPath();
        draw.clearRect(0,0,canvas.width,canvas.height);
        var canvasImage = new Image();
        canvasImage.src = canvasArray[step];
        draw.drawImage(canvasImage,0,0);

        draw.arc((cursor.clientX + startX-400)/2, (cursor.clientY + startY-26)/2, Math.sqrt(Math.pow((cursor.clientX - startX),2) + Math.pow((cursor.clientY - startY),2))/2, 0, 2*Math.PI);
        draw.stroke();
    }

    if(drawType == "triangle"){
        draw.lineWidth = document.getElementById("brushSize").value;

        draw.beginPath();
        draw.clearRect(0,0,canvas.width,canvas.height);
        var canvasImage = new Image();
        canvasImage.src = canvasArray[step];
        draw.drawImage(canvasImage,0,0);

        draw.moveTo(startX-200,startY-13);
        draw.lineTo(cursor.clientX-200,cursor.clientY-13);
        draw.lineTo(cursor.clientX+(cursor.clientX - startX)-200,startY-13);
        draw.closePath();
        draw.stroke();
    }

    if(drawType == "filledrect"){
        draw.lineWidth = document.getElementById("brushSize").value;

        draw.beginPath();
        draw.clearRect(0,0,canvas.width,canvas.height);
        var canvasImage = new Image();
        canvasImage.src = canvasArray[step];
        draw.drawImage(canvasImage,0,0);

        draw.rect(startX-200,startY-13,cursor.clientX-startX,cursor.clientY-startY);
        draw.fill();
        draw.stroke();
    }

    if(drawType == "filledcir"){
        draw.lineWidth = document.getElementById("brushSize").value;
        draw.lineCap = "round";

        draw.beginPath();
        draw.clearRect(0,0,canvas.width,canvas.height);
        var canvasImage = new Image();
        canvasImage.src = canvasArray[step];
        draw.drawImage(canvasImage,0,0);

        draw.arc((cursor.clientX + startX-400)/2, (cursor.clientY + startY-26)/2, Math.sqrt(Math.pow((cursor.clientX - startX),2) + Math.pow((cursor.clientY - startY),2))/2, 0, 2*Math.PI);
        draw.fill();
        draw.stroke();
    }

    if(drawType == "filledtri"){
        draw.lineWidth = document.getElementById("brushSize").value;

        draw.beginPath();
        draw.clearRect(0,0,canvas.width,canvas.height);
        var canvasImage = new Image();
        canvasImage.src = canvasArray[step];
        draw.drawImage(canvasImage,0,0);

        draw.moveTo(startX-200,startY-13);
        draw.lineTo(cursor.clientX-200,cursor.clientY-13);
        draw.lineTo(cursor.clientX+(cursor.clientX - startX)-200,startY-13);
        draw.closePath();
        draw.fill();
        draw.stroke();
    }
}

function downloadImage(){
    var download = document.getElementById("download");
    var image = document.getElementById("mycanvas").toDataURL("image/png").replace("image/png", "image/octet-stream");
    download.setAttribute("href", image);
}

function upload(){
    var upload = document.getElementById("upload");
    upload.click();
}

function uploadImage(event){
    var image = new Image();
    image.src = URL.createObjectURL(event.target.files[0]);
    image.onload = function(){
        draw.clearRect(0,0,canvas.width,canvas.height);
        draw.drawImage(image,600,200);
    }
}

function SetPencil(){
    drawType = "pencil";
    canvas.style.cursor = "url('./pencil.cur'), auto";
    console.log("switched to pencil");
}

function SetEraser(){
    drawType = "eraser";
    console.log("switched to eraser");
}

function SetLine(){
    drawType = "line";
    console.log("switched to line");
}

function SetRectangle(){
    drawType = "rect";
    console.log("switched to rectangle");
}

function SetCircle(){
    drawType = "circle";
    console.log("switched to circle");
}

function SetTriangle(){
    drawType = "triangle";
    console.log("switched to triangle");
}

function SetFilledRectangle(){
    drawType = "filledrect";
    console.log("switched to filledrectangle");
}

function SetFilledCircle(){
    drawType = "filledcir";
    console.log("switched to filledcircle");
}

function SetFilledTriangle(){
    drawType = "filledtri";
    console.log("switched to filledtriangle");
}

function typeWriter(words){
    var text = words.value;
    var style = document.getElementById("fontstyle").value;
    var size = document.getElementById("fontsize").value;
    console.log(style, size);
    text.font = size + 'px ' + style;
    draw.fillText(text,600,200);
}
